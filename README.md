# manual-start

A small Python Tkinter app I made a few months ago in an hour by watching this video https://www.youtube.com/watch?v=jE-SpRI3K5g, where I also tweaked some things, primarily to make the GUI serviceable and not take up the entire screen for no reason

## What it does

You can place it on your desktop to quickly launch it and click on the "Run Apps" button to manually start the apps you've chosen, like a bunch of apps that you use for development. It reads and saves the paths to apps that you've chosen into a "save.txt" file that is created in the same directory. So it's better to create a new directory and put the executable there.

So it's basically a glorified batch script that can be set to launch on startup via Task Scheduler. The only difference is that you will not need to go through the incredibly tedious process of writing the path/to/.exe in Notepad whenever you want to add another program to launch on startup, so there's a nice quality-of-life feature. /s

## But where's the source code?

I don't know where it is anymore, after I got myself a new laptop, and I don't care to look for it. Either way, the goal of this was to learn how to make your own apps, so if you want to know how to write an app like this on your own (or steal someone else's code), just watch the video I linked above and take a few minutes to improve the app

I'd appreciate it if you just made a branch with roughly the same code (and maybe even some extra quality-of-life features) that I can merge with, but I have zero desire to continue what is essentially a homework assignment in a university (we never learned Python there, since it's a toy language for 12-year-olds). It already works decently and there are dozens of thousands of much better open-source apps that do the same task much better
